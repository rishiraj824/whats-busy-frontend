import Head from 'next/head';
const Header =() =>(
    <Head>
    <title>WhatsBusy Frontend Challenge</title>
    <link rel="icon" href="/favicon.ico" />
    <script src="https://www.amcharts.com/lib/version/4.9.19/core.js"></script>
<script src="https://www.amcharts.com/lib/version/4.9.19/charts.js"></script>
  </Head>)

  export default Header;
