import { Component } from "react";
import { readRemoteFile } from 'react-papaparse';
import Header from '../components/header';
import { getHumanDate } from '../utils';

let am4core = null
let am4charts = null
let am4themesAnimated = null
if (process.browser) {
  am4core = require('@amcharts/amcharts4/core')
  am4charts = require('@amcharts/amcharts4/charts')
  am4themesAnimated = require('@amcharts/amcharts4/themes/animated')
  am4core.useTheme(am4themesAnimated.default)
}



export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      station: '101',
      stationCodes: [],
      dataSet: {},
      dateRange: {
        start: new Date('2017-05-01'),
        end: new Date('2017-12-01')
      }
    }
  }
  componentDidMount(){
    this.getData();
  }
  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }
  // Create series
createSeries = (chart, field, name) => {

  var series = chart.series.push(new am4charts.LineSeries());

  series.dataFields.dateY = "Measurement date";
  series.dataFields.valueX = field;
  series.name = name;
  series.tooltipText = `${field}: [b]{valueX}[/]`;
  series.strokeWidth = 2;
  return series;
}
  plot = (data, start, end) => {
    let chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.paddingRight = 20;
    
    
    chart.data = data.filter(row=>{
      if (row['Measurement date'] <= end && row['Measurement date'] >= start) {
        return row
      }
    })

    let dateAxis = chart.yAxes.push(new am4charts.DateAxis());
    let valueAxes = chart.xAxes.push(new am4charts.ValueAxis());

    dateAxis.renderer.grid.template.location = 0;

    let series_SO2 = this.createSeries(chart, 'SO2', 'SO2');
    let series_NO2 = this.createSeries(chart, 'NO2', 'NO2');
    let series_O3 =this.createSeries(chart, 'O3', 'O3');
    let series_CO = this.createSeries(chart, 'CO', 'CO');

    chart.cursor = new am4charts.XYCursor();
    let scrollbarY = new am4charts.XYChartScrollbar();
    scrollbarY.series.push(series_SO2);
    scrollbarY.series.push(series_NO2);
    scrollbarY.series.push(series_O3);
    scrollbarY.series.push(series_CO);

    chart.scrollbarY = scrollbarY;
    chart.legend = new am4charts.Legend();
    chart.responsive.enabled = true;
    this.chart = chart;
  }
  
  getData =()=>{
    this.setState({
      loading:true
    })

    let { dataSet: dataSetMap, stationCodes = [] } = this.state;
    const options = {
      delimitersToGuess: [' ', ','],
      quoteChar: '"',
      worker: true,
      newline: '\n',
      encoding: 'utf-8',
      chunk: (chunkData) => {
          let chunk = chunkData.data;
        chunk.map(row=>{

          const date = new Date(row['Measurement date']);
          const stationCode = row['Station code'];
          const dataPoint = {
            'NO2': row['NO2'],
            'SO2': row['SO2'],
            'CO': row['CO'],
            'O3': row['O3'],
            'Measurement date': date
          }
          if(dataSetMap[stationCode]){
            dataSetMap[stationCode].push(dataPoint)
          }
          else {
            stationCodes.push(stationCode);
            dataSetMap[stationCode] = []
          }

        })
      },
      error: (err) => {
          console.log(err)
      },
      skipEmptyLines: true,
      header: true,
      complete: (results, file) => {
          console.log("Parsing complete");
          this.setState({
            stationCodes,
            dataSet: dataSetMap,
            station: stationCodes[0],
            loading: false
          })
          this.plot(dataSetMap[stationCodes[0]], this.state.dateRange.start, this.state.dateRange.end);
      }
  };
        readRemoteFile(
          `${window.location.origin}/Measurement_summary.csv`,
          options
        )
  }

  getStationCodes = () => this.state.stationCodes.map(station=>{
      return <option key={station} value={station}>{station}</option>
    })

  onChangeStationCode = (e) => {
    const { dataSet, dateRange } = this.state;
    this.setState({
      station: e.target.value,
      data: dataSet[e.target.value]
    })
    this.plot(dataSet[e.target.value], dateRange.start, dateRange.end)
  }
  onDateChange=(e)=>{
    const dateRange = {
      ...this.state.dateRange,
      [e.target.name]: new Date(e.target.value)
    }
    this.setState({
      dateRange
    },()=>{
      this.plot(this.state.dataSet[this.state.station], this.state.dateRange.start, this.state.dateRange.end);
    })
  }
  render(){
    const { loading, station, dateRange } = this.state;
    return (
  <div className="container">
  <Header/>
    <main>
      <div className="hero">
      <h3>Air Pollution in Seoul</h3>
      <div className="filter-container">
        <div className="filters"><label>Station Code:</label><select className="select input" value={station} onChange={this.onChangeStationCode}>{this.getStationCodes()}</select>
        </div>
        <div className="filters"><label>Start</label><input name="start" type="date" onChange={this.onDateChange} value={getHumanDate(dateRange.start)} className="input" />
        </div>
        <div className="filters"><label>End</label><input name="end" type="date" onChange={this.onDateChange} value={getHumanDate(dateRange.end)} className="input" />
        </div>
      </div>
      </div>
      <div id="chartdiv" className="chart-container">{loading&&<div className="loading-text"><h3>Loading...</h3></div>}</div>
    </main>
      <style jsx>
        {`
        @media screen and (max-width:786px){
          .filters {
            margin: 0.4rem 0;
          }

        .hero{
          height: 18vh !important;
        }
      }
        .chart-container {
          width: 100%;
          height: 500px;
          background: #f2f2f2;
          margin-top: 2em;
        }
        .hero h3 {
          line-height: 5vh;
          margin-bottom:0.4rem;
        }
        .hero{
          height: 15vh;
          width: 100%;
          position:relative;
          margin:auto;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
        }
        .filter-container {
          display: flex;
          justify-content: center;
          align-items: center;
          flex-wrap:wrap;
        }
        
        .input {
          outline: 0;
          padding-right: 0;
          -webkit-appearance:none;
          border-radius: 0;
          border: 1px solid #4d4d4d;
          background: #4d4d4d;
          color: #fff;
          padding: 0 0.4rem;
          height: 30px;
          font-size: 1em;
          margin: 0 0.5em;
          width: 155px;
        }
        .loading-text {
          display: flex;
          justify-content:center;
          color: #4d4d4d;
          font-weight: 600;
          font-size: 1.2em;
          line-height: 1.8;
          padding: 0.8em 2em;
        }
        .button:hover{
          opacity: 0.7;
        }`}
      </style>
    <style jsx global>{`
      html,
      body {
        padding: 0;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
          Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
      }
      input {

        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
          Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
      }
      footer {
        text-align:center;
        padding: 1rem;
      }

      * {
        box-sizing: border-box;
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
          Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
      }
    `}</style>
  </div>
)
    }
  }

